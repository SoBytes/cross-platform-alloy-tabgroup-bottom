// additions to the file
var tabsHandler = function(_event) {
	resetButtons($.tabgroup.children);
	_event.source.setBackgroundColor('#2F7392');
	$.fakeTitle.text = _event.source.title;
	$.pages.setCurrentPage(_event.source.index);
};
$.tabgroup.addEventListener('click', tabsHandler);

var hideTabsIn = Ti.UI.createAnimation({
	bottom : "0dp",
	duration : 100
});

var scaleWinIn = Ti.UI.createAnimation({
	bottom : "60dp",
	duration : 150
});

var hideTabsOut = Ti.UI.createAnimation({
	bottom : "-60dp",
	duration : 100
});

var scaleWinOut = Ti.UI.createAnimation({
	bottom : "0dp",
	duration : 150
});

function openNav() {
	$.tabgroup.animate(hideTabsOut, function() {
		//alert("Its done!!!");
		Alloy.createController('newwin').getView();
		//$.win1.openWindow(win3);
		//$.newwin.open();
	});
	$.pages.animate(scaleWinOut, function() {
		//alert("Its done!!!");
	});
}

function openMe() {
	alert("Open main nav");
}

/*
 * Remove all the background states
 */
function resetButtons(_views) {
	for ( i = 0; i < _views.length; i++) {
		_views[i].setBackgroundColor('#EEEFF1');
	}
}

/*
 * Setup the tabs groups
 */
var setupTabs = [{
	title : "Samuel one",
	focus : true
}, {
	title : "Samuel two"
}, {
	title : "Samuel three"
}, {
	title : "Samuel four"
}];

var numberOfTabs = 4;
var tabsWidths = Alloy.Globals.Device.width / numberOfTabs;

for ( i = 0; i < setupTabs.length; i++) {

	var view = Titanium.UI.createView({
		//backgroundColor : (setupTabs[i].focus) ? '#2F7392' : '#EEEFF1',
		backgroundImage : '/images/bg.png',
		index : i,
		title : setupTabs[i].title,
		left : 0,
		top : "15dp",
		height : "60dp",
		width : tabsWidths,
		clipMode : Titanium.UI.iOS.CLIP_MODE_DISABLED,
		zIndex : 1
	});

	/*view.add(Ti.UI.createImageView({
	 touchEnabled : false,
	 height : Ti.UI.SIZE,
	 width : Ti.UI.FILL,
	 image : '/images/bg.png'
	 }));*/

	if ((setupTabs[i].focus)) {
		view.add(Ti.UI.createLabel({
			backgroundColor : "#F14346",
			touchEnabled : false,
			color : '#EEEFF1',
			font : {
				fontSize : "9sp"
			},
			text : '12',
			textAlign : Ti.UI.TEXT_ALIGNMENT_CENTER,
			top : "-15dp",
			width : "30dp",
			height : "30dp",
			borderRadius : "15dp",
			zIndex : 2
		}));
	}

	$.tabgroup.add(view);

}

function focusWindow() {
	console.log("Yes we are running focus.");
	$.tabgroup.animate(hideTabsIn, function() {

	});
	$.pages.animate(scaleWinIn, function() {

	});
}

$.index.open();
