var args = arguments[0] || {};

function closeBlueWindow(){
	$.newwin.close();
}

function openBlueWindow(){
	var win3 = Alloy.createController('bluewin').getView();
    $.newwin.openWindow(win3);
}

$.newwin.open();